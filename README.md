# Tutorial 4

For this tutorial, we're going to introduce Webpack 

And, yes, we're still working with Conway's Game of Life.

## Clone the code

First, get the code and put it somewhere you can load it in your browser. 

To get the code, open a terminal, `cd` into the public directory of whichever webserver you are using, and:

```sh
git clone https://gitlab.une.edu.au/cosc360in2018/tutorial-conway-life-t4.git
```

This will clone this code using the git version control system. That will, amongst other things, let you switch between different branches (eg, the solution and the start of the exercise)

## Check it's working

Open `index.html` in the browser, and you should find yourself faced with Conway's Game of Life from Tutorial 3.

## Set up Webpack

Open a terminal in the `tutorial-conway-life-t4` directory.

1. Install webpack, and loaders for typescript and vue

    ```sh
    npm install --save-dev webpack ts-loader css-loader vue vue-loader vue-template-compiler
    ```

2. Let's also install Vue's TypeScript-class like component definitions in case you'd like to use those

    ```sh
    npm install --save-dev vue-class-component vue-property-decorator
    ```

3. Create a src directory, and move the typescript files into it

    ```sh
    mkdir src
    mv *.ts src/
    ```

4. Edit `tsconfig.json` to use ECMA2015 as its module loader, node to resolve modules, and to turn on experimental decorators. Also ensure it looks for sources in the `src` subtree.

    ```json
    {
        "compilerOptions": {
            "outDir": "./built/",
            "sourceMap": true,
            "strict": true,
            "noImplicitReturns": true,
            "module": "es2015",
            "moduleResolution": "node",
            "target": "es5",
            "experimentalDecorators": true
        },
        "include": [
            "./src/**/*"
        ]
    }
    ```

5. Now, let's create `webpack.config.js`. Take a look at the following and see what it does.

    ```js
    const path = require('path');

    module.exports = {
        entry: './src/index.ts',
        module: {
            rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
            ]
        },
        resolve: {
            extensions: [ '.tsx', '.ts', '.js' ]
        },
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'dist')
        }
    };
    ```

6. Before we can run webpack, we're going to need to install its command-line interface. There are two, so let's install the official one

    ```sh
    npm install webpack-cli
    ```

7. If we run webpack, via `npx webpack` we'll find we get an error. We don't actually have a `src/index.ts` file. So, let's create it and put in it the contents from the script tag in our html:

    ```ts
    render()
    ```

   We should now find that if we run `npx webpack` it generates a single js file `dist/build.js`

   Replace the script tag containing the call to `render()` in `index.html` with a script tag to load this file.

   Now reload the page, and the code should still work.
   
   Note: some students who've set up npm on their own machine might find they don't have npx (at least, one in the tutorial did). 
   If so, just imagine you've run npx webpack and keep on going to step 8 -- we won't usually be using the npx command to run
   webpack anyway.

8. Next, let's tell NPM that we build our project using webpack. In `package.json`, inside the `scripts` block, add

    ```json
    "build": "webpack",
    ```

    We can now tell npm to watch our project for changes, and rebuild it automatically:

    ```sh
    npm run build -- --watch
    ```

9. Next, let's start using TypeScript modules. First, we should actually install d3, rather than only its types. If we're using modules, we can't also use globals.

    ```sh
    npm install --save-dev d3
    ```

    And let's alter our index.html to give the button and id, and remove the onclick that relies on a global.

    ```html
    <button id="step" class="btn btn-primary" >Step</button>
    ```

10. In `src/gameOfLife.ts`, let's change the top line to:

    ```ts
    export default class Life {
    ```

    and in `src/render.ts`

    ```ts
    import Life from "./gameOfLife"
    import * as d3 from "d3"
    ```

    In render.ts, we'll need to export both the `life` constant and the `render`
    function. Because we have two names to export, rather than use `export default`
    we'll need to insert an explicit export block

    ```ts
    export {
      life as life,
      render as render
    }
    ```

    and in `src/index.ts`

    ```ts
    import { render, life } from "./render"

    render()

    document.getElementById("step")!.addEventListener("click", () => {
        life.stepGame()
        render()
    })
    ```

All going well, if you rebuild the code and then refresh the browser, Life is now working using TypeScript modules and webpack.

### Vue, React, Angular?

Next week, the tutorial solution will be aroudn a challenge: above the d3-rendered 
game of life, add some controls using your preferred client-side framework that 
will allow you to alter the board's dimensions (resetting the game) and that 
will allow you to start and stop a tick-timer moving the game forward automatically.

But for the moment, you have videos to critique, and should get started writing
the TypeScript model of your own app.