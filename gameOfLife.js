"use strict";
var Life = /** @class */ (function () {
    function Life(w, h) {
        if (w === void 0) { w = 40; }
        if (h === void 0) { h = 20; }
        this.w = w;
        this.h = h;
        this.board = this.emptyBoard();
    }
    Life.prototype.boardIndex = function (x, y) {
        return y * this.w + x;
    };
    Life.prototype.x = function (index) {
        return index % this.w;
    };
    Life.prototype.y = function (index) {
        return Math.floor(index / this.w);
    };
    Life.prototype.emptyBoard = function () {
        var arr = [];
        for (var i = 0; i < this.h * this.w; i++) {
            arr.push(false);
        }
        return arr;
    };
    Life.prototype.toggleCell = function (x, y) {
        var b = this.boardIndex(x, y);
        this.board[b] = !this.board[b];
    };
    Life.prototype.isAlive = function (x, y) {
        var b = this.boardIndex(x, y);
        return (x >= 0) && (y >= 0) && (x < this.w) && (y < this.h)
            && this.board[b];
    };
    Life.prototype.liveNeighbours = function (x, y) {
        var live = 0;
        for (var _i = 0, _a = [x - 1, x, x + 1]; _i < _a.length; _i++) {
            var i = _a[_i];
            for (var _b = 0, _c = [y - 1, y, y + 1]; _b < _c.length; _b++) {
                var j = _c[_b];
                if (this.isAlive(i, j) && (i != x || j != y)) {
                    live++;
                }
            }
        }
        return live;
    };
    Life.prototype.stepGame = function () {
        var nextBoard = this.emptyBoard();
        for (var x = 0; x < this.w; x++) {
            for (var y = 0; y < this.h; y++) {
                var index = this.boardIndex(x, y);
                var alive = this.isAlive(x, y);
                var neighbours = this.liveNeighbours(x, y);
                if ((alive && (neighbours == 2 || neighbours == 3)) ||
                    (!alive && neighbours == 3)) {
                    nextBoard[index] = true;
                }
                else {
                    nextBoard[index] = false;
                }
            }
        }
        this.board = nextBoard;
    };
    return Life;
}());
//# sourceMappingURL=gameOfLife.js.map