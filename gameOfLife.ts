
class Life {

    board: Array<boolean>

    constructor(public w:number = 40, public h:number = 20) {
        this.board = this.emptyBoard()
    }

    boardIndex(x:number, y:number):number {
        return y * this.w + x
    }

    x(index:number) {
        return index % this.w
    }

    y(index:number) {
        return Math.floor(index / this.w)
    }

    emptyBoard():boolean[] {
        let arr = <boolean[]>[]
        for (let i = 0; i < this.h * this.w; i++) {
            arr.push(false)
        }
        return arr
    }

    toggleCell(x:number, y:number):void {
        let b = this.boardIndex(x,y)
        this.board[b] = !this.board[b]
    }

    isAlive(x:number, y:number):boolean {
        let b = this.boardIndex(x,y)
        return (x >= 0) && (y >= 0) && (x < this.w) && (y < this.h) 
          && this.board[b]
    }

    liveNeighbours(x:number, y:number) {
        let live = 0
        for (let i of [x - 1, x, x + 1]) {
            for (let j of [y - 1, y, y + 1]) {
                if (this.isAlive(i,j) && (i != x || j != y)) {
                    live++
                }
            }
        }
        return live
    }

    stepGame():void {
        let nextBoard = this.emptyBoard()
        for (let x = 0; x < this.w; x++) {
            for (let y = 0; y < this.h; y++) {
                let index = this.boardIndex(x, y)
                let alive = this.isAlive(x, y)
                let neighbours = this.liveNeighbours(x, y)

                if (
                    (alive && (neighbours == 2 || neighbours == 3)) ||
                    (!alive && neighbours == 3)
                ) {
                    nextBoard[index] = true
                } else {
                    nextBoard[index] = false
                }    
            }
        }
        this.board = nextBoard
    }

}



