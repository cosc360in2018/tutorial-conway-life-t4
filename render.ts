/// <reference types="d3" />

const life = new Life()
const game = d3.select("#game").append("g")
const cellSize = 20

function render() {
    console.log("rendering")
    
    let update = game.selectAll("rect").data(life.board)
      
    update.attr("class", (d) => {
        return d ? "cell alive" : "cell"            
    })
      
    update.enter()
        .append("rect")
        .attr("class", (d) => {
            return d ? "cell alive" : "cell"            
        })
        .attr("x", (val,i) => {
          return life.x(i) * cellSize
        })
        .attr("y", (val,i) => {
          return life.y(i) * cellSize
        })
        .attr("width", cellSize)
        .attr("height", cellSize)
        .on("click", (d,i) => {
            let x = life.x(i)
            let y = life.y(i)
            life.toggleCell(x,y)
            render()
        })
  

}